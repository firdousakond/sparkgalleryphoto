package com.firdous.galleryimageupload

import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.firdous.galleryimageupload.activities.ImagePickerActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ImagePickerActivityUITest {

    @get:Rule
    val activityTestRule = ActivityTestRule(ImagePickerActivity::class.java)

    @Test
    fun activityLaunchesSuccessfully(){
        ActivityScenario.launch(ImagePickerActivity::class.java)
    }

}