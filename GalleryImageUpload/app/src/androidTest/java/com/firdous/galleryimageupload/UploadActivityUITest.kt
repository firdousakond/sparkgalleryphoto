package com.firdous.galleryimageupload

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.firdous.galleryimageupload.activities.ImagePickerActivity
import com.firdous.galleryimageupload.activities.UploadImageActivity
import com.firdous.galleryimageupload.viewmodels.UploadImageViewModel
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UploadActivityUITest {

    @get:Rule
    val activityTestRule = ActivityTestRule(UploadImageActivity::class.java)

    @Test
    fun activityLaunchesSuccessfully(){
        ActivityScenario.launch(UploadImageActivity::class.java)
    }

    @Test
    fun checkUploadButtonVisible(){
        onView(withId(R.id.btn_upload_photo)).check(matches(isDisplayed()))
    }

    @Test
    fun checkProfileImageVisible(){
        onView(withId(R.id.img_profile)).check(matches(isDisplayed()))
    }

    @Test
    fun checkEditIconVisible(){
        onView(withId(R.id.img_edit)).check(matches(isDisplayed()))
    }

    @Test
    fun pickImageIconClick(){
        onView(withId(R.id.img_edit)).perform(click())
        ActivityScenario.launch(ImagePickerActivity::class.java)
    }

    @Test
    fun uploadButtonClick(){
        onView(withId(R.id.btn_upload_photo)).perform(click())
    }

    @Test
    fun checkBackButtonVisible(){
        onView(withId(R.id.ibtn_back)).check(matches(isDisplayed()))
    }
    @Test
    fun checkHeaderTextVisible(){
        onView(withId(R.id.txt_header)).check(matches(isDisplayed()))
    }
    @Test
    fun backButtonClick(){
        onView(withId(R.id.ibtn_back)).perform(click())
    }

}