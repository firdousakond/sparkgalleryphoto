package com.firdous.galleryimageupload

import android.view.View
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.firdous.galleryimageupload.activities.MainActivity
import com.firdous.galleryimageupload.activities.UploadImageActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.matcher.ViewMatchers.*
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.contrib.RecyclerViewActions
import com.firdous.galleryimageupload.activities.FullImageActivity
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matcher


@RunWith(AndroidJUnit4::class)
class MainActivityUITest {

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun activityLaunchesSuccessfully() {
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun checkAddButtonVisibility(){
        onView(withId(R.id.btn_add_photo)).check(matches(isDisplayed()))
    }
    @Test
    fun checkRecyclerViewVisibility(){
        onView(withId(R.id.rv_images)).check(matches(isDisplayed()))
    }

    @Test
    fun addNewImageClick(){
        onView(withId(R.id.btn_add_photo)).perform(click())
        ActivityScenario.launch(UploadImageActivity::class.java)
    }

   @Test
   fun checkRecyclerListEmpty(){

       onView(withId(R.id.rv_images)).check(matches(hasChildCount(0)))
       onView(withId(R.id.txt_no_data)).perform(setTextViewVisibility(true))
   }

    @Test
    fun recyclerItemClick(){
        val recyclerView : RecyclerView = activityTestRule.activity.findViewById(R.id.rv_images)
        if(recyclerView.adapter!!.itemCount > 0){
            onView(withId(R.id.rv_images)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()));
            ActivityScenario.launch(FullImageActivity::class.java)
        }
    }

    @Test
    fun checkBackButtonVisible(){
        onView(withId(R.id.ibtn_back)).check(matches(not(isDisplayed())))
    }
    @Test
    fun checkHeaderTextVisible(){
        onView(withId(R.id.txt_header)).check(matches(isDisplayed()))
    }

    private fun setTextViewVisibility(value: Boolean): ViewAction {
        return object : ViewAction {

            override fun getConstraints(): Matcher<View> {
                return isAssignableFrom(TextView::class.java)
            }

            override fun perform(uiController: UiController, view: View) {
                view.visibility = if (value) View.VISIBLE else View.GONE
            }

            override fun getDescription(): String {
                return "Show / Hide View"
            }
        }
    }

}