package com.firdous.galleryimageupload

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.firdous.galleryimageupload.activities.FullImageActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FullImageActivityUITest {

    @get:Rule
    val activityTestRule = ActivityTestRule(FullImageActivity::class.java)

    @Test
    fun activityLaunchesSuccessfully(){
        ActivityScenario.launch(FullImageActivity::class.java)
    }

    @Test
    fun checkImageVisible(){
        onView(withId(R.id.img_profile)).check(matches(isDisplayed()))
    }
    @Test
    fun checkBackButtonVisible(){
        onView(withId(R.id.ibtn_back)).check(matches(isDisplayed()))
    }
    @Test
    fun checkHeaderTextVisible(){
        onView(withId(R.id.txt_header)).check(matches(isDisplayed()))
    }
    @Test
    fun backButtonClick(){
        onView(withId(R.id.ibtn_back)).perform(click())
    }
}