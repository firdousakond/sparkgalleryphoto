package com.firdous.galleryimageupload.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.verify

class UploadViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: UploadImageViewModel
    @Mock
    private lateinit var databaseReference: DatabaseReference
    @Mock
    private lateinit var storageReference: StorageReference

    @Mock
    private lateinit var uploadReference: StorageReference

    @Mock
    private lateinit var progressObserver: Observer<Double>

    @Mock
    private lateinit var uploadTask : UploadTask.TaskSnapshot

    private val progress = 50.0

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        viewModel = UploadImageViewModel(databaseReference, storageReference)

    }

   @Test
   fun uploadProgressTest(){

       viewModel.progressUpdate.observeForever(progressObserver)
       viewModel.progressUpdate.value = progress
       viewModel.setUploadProgress(uploadTask)
       verify(progressObserver).onChanged(progress)

   }

}