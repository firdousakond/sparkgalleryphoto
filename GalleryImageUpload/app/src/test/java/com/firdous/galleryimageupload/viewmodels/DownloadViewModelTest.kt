package com.firdous.galleryimageupload.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.firdous.galleryimageupload.model.UploadModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


class DownloadViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var viewModel : DownloadImageViewModel
    @Mock
    private lateinit var responseData : List<UploadModel>
    @Mock
    private lateinit var dataSnapshot: DataSnapshot
    @Mock
    private lateinit var databaseError: DatabaseError
    @Mock
    private lateinit var databaseReference: DatabaseReference
    @Captor
    private lateinit var argumentCaptor : ArgumentCaptor<ValueEventListener>
    @Mock
    private lateinit var successObserver: Observer<List<UploadModel>>
    @Mock
    private lateinit var failureObserver: Observer<String>

    private val errorMessage = "errorOnDownload"


    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        viewModel = DownloadImageViewModel(databaseReference)
    }

    @Test
    fun downloadImageSuccessTest(){
        viewModel.uploadModelList.observeForever(successObserver)
        viewModel.uploadModelList.value = responseData
        viewModel.fetchImages()
        verify(databaseReference).addValueEventListener(argumentCaptor.capture())
        argumentCaptor.value.onDataChange(dataSnapshot)
        verify(successObserver).onChanged(responseData)
    }

    @Test
    fun downloadImageFailureTest(){

        viewModel.errorMessage.observeForever(failureObserver)
        viewModel.errorMessage.value = errorMessage
        viewModel.fetchImages()
        verify(databaseReference).addValueEventListener(argumentCaptor.capture())
        argumentCaptor.value.onCancelled(databaseError)
        verify(failureObserver).onChanged(errorMessage)
    }

}