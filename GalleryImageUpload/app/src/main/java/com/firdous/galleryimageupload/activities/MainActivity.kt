package com.firdous.galleryimageupload.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.firdous.galleryimageupload.R
import com.firdous.galleryimageupload.adapter.ImageAdapter
import com.firdous.galleryimageupload.utils.AppConstant
import com.firdous.galleryimageupload.utils.CommonUtils
import com.firdous.galleryimageupload.viewmodels.DownloadImageViewModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_image_list.*
import kotlinx.android.synthetic.main.activity_toolbar.*

class MainActivity : AppCompatActivity() {

    private lateinit var imageAdapter : ImageAdapter
    private lateinit var viewModel: DownloadImageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_list)
        val databaseReference = FirebaseDatabase.getInstance().getReference(AppConstant.UPLOADS)
        viewModel = ViewModelProviders.of(this, CustomViewModelFactory(databaseReference)).get(DownloadImageViewModel::class.java)
        initUI()
        setListener()
    }

    /**
     * Observe the response from firebase db
     */
    override fun onResume() {
        super.onResume()
        viewModel.errorMessage.observe(this, Observer {
            if(it != null) showError(it)
        })
        fetchImageList()
    }

    private fun initUI(){
        txt_header.text = getString(R.string.photo_gallery)

        rv_images.layoutManager = GridLayoutManager(this,2)
        imageAdapter = ImageAdapter(this)
        rv_images.adapter =imageAdapter

    }

    private fun showError(errorMessage : String){
        progress_circular.visibility = View.GONE
        CommonUtils.displaySnackbar(this,errorMessage,AppConstant.MESSAGE_TYPE_ERROR)
    }

    private fun fetchImageList(){
        if(!CommonUtils.isNetworkAvailable(this)){
            CommonUtils.displaySnackbar(this,getString(R.string.msg_no_internet), AppConstant.MESSAGE_TYPE_ERROR)
            return
        }
        progress_circular.visibility = View.VISIBLE
        viewModel.fetchImages()
        viewModel.uploadModelList.observe(this, Observer {
            progress_circular.visibility = View.GONE
            if(it.isEmpty()){
                txt_no_data.visibility = View.VISIBLE
            }
            else {
                txt_no_data.visibility = View.GONE
                imageAdapter.updateImageList(it.asReversed())
            }
        })
    }

    private fun setListener(){
        btn_add_photo.setOnClickListener { gotoUploadImageScreen() }
    }

    private fun gotoUploadImageScreen() {
        val intent = Intent(this,UploadImageActivity::class.java)
        startActivity(intent)
    }

    inner class CustomViewModelFactory (private val databaseReference: DatabaseReference): ViewModelProvider.Factory{

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return DownloadImageViewModel(databaseReference) as T
        }
    }

}
