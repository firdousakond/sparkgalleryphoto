package com.firdous.galleryimageupload.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.firdous.galleryimageupload.R;
import com.firdous.galleryimageupload.permission.AlertDialogProperties;
import com.firdous.galleryimageupload.permission.AppPermission;

import java.util.ArrayList;

import static com.firdous.galleryimageupload.permission.PermissionUtil.MULTIPLE_PERMISSION_REQUEST;

public class AbstractActivity extends AppCompatActivity {

    private AlertDialogProperties alertDialogProperties;
    protected AbstractActivity activity;
    private String[] permissionArray;
    protected AppPermission appPermission;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPermission = AppPermission.getInstance();
    }

    protected void showPermissionAlert(boolean toCloseActivity, boolean toExitFromApp) {
        initAlertDialog();
        alertDialogProperties.setOkayButtonText(getString(R.string.ok));
        alertDialogProperties.setOkayButtonListener(new OkButtonListener());
        alertDialogProperties.setCancelButtonListener(new CancelButtonListener(toCloseActivity, toExitFromApp));
    }

    private void initAlertDialog() {
        alertDialogProperties = new AlertDialogProperties();
        alertDialogProperties.setButtonTextColor(Color.WHITE);
        alertDialogProperties.setCancelable(false);
        alertDialogProperties.setMessageTextColor(Color.BLACK);
        alertDialogProperties.setCancelButtonText(getString(R.string.exit));
    }

    protected void checkPermissionAndShowPopup(ArrayList<String> permissionList) {
        String[] tempArray = new String[permissionList.size()];
        permissionList.toArray(tempArray);
        permissionList.clear();
        permissionArray = tempArray;
        if (activity != null && !activity.isFinishing()) {
            appPermission.showPermissionDialog(activity, alertDialogProperties, permissionArray);
        }
    }

    protected void showAlertToGoSetting(boolean toCloseActivity, boolean toExitFromApp) {
        initAlertDialog();
        alertDialogProperties.setOkayButtonText(getString(R.string.app_settings));
        alertDialogProperties.setOkayButtonListener(new GoToSettings(toCloseActivity, toExitFromApp));
        alertDialogProperties.setCancelButtonListener(new CancelButtonListener(toCloseActivity, toExitFromApp));
    }

    private class GoToSettings implements View.OnClickListener {

        final boolean toCloseActivity;
        final boolean toExitFromApp;

        GoToSettings(boolean toCloseActivity, boolean toExitFromApp) {
            this.toCloseActivity = toCloseActivity;
            this.toExitFromApp = toExitFromApp;
        }

        private void navigateToAppSetting() {
            Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + activity.getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(i);
        }

        @Override
        public void onClick(View v) {
            navigateToAppSetting();
            appPermission.closeDialog();

            if (toCloseActivity) {
                onBackPressed();
            }
        }
    }

    private class OkButtonListener implements View.OnClickListener {


        @Override
        public void onClick(View view) {

            appPermission.requestPermissionList(activity, permissionArray, MULTIPLE_PERMISSION_REQUEST);
            appPermission.closeDialog();
        }
    }

    private class CancelButtonListener implements View.OnClickListener {

        final boolean toCloseActivity;
        final boolean toExitFromApp;

        CancelButtonListener(boolean toCloseActivity, boolean toExitFromApp) {
            this.toCloseActivity = toCloseActivity;
            this.toExitFromApp = toExitFromApp;
        }

        @Override
        public void onClick(View v) {
            appPermission.closeDialog();

            if (toExitFromApp) {
                finishAffinity();
            }

            if (toCloseActivity) {
                onBackPressed();
            }

        }
    }

    protected boolean isNetworkAvailable(Context context) {

        if(context != null) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connectivity != null) {

                NetworkInfo[] info = connectivity.getAllNetworkInfo();

                if (info != null) {

                    for (int i = 0; i < info.length; i++) {

                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {

                            return true;
                        }
                    }
                }
            }
        }

        return false;

    }

}
