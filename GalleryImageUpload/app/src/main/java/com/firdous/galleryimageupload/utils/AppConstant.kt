package com.firdous.galleryimageupload.utils

class AppConstant private constructor() {

    companion object {
        const val IMAGE_FILE = "IMAGE_FILE"
        const val FILE_EXTENSION = "FILE_EXTENSION"
        const val IMAGE_URL = "IMAGE_URL"
        const val IMAGE_PICKER_CODE = 1000
        const val CONSTANT_GLIDE_TIMEOUT = 120000
        const val UPLOADS = "uploads"
        const val MESSAGE_TYPE_SUCCESS = 100
        const val MESSAGE_TYPE_ERROR = 200
    }

}
