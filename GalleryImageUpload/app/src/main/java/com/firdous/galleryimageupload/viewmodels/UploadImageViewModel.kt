package com.firdous.galleryimageupload.viewmodels

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.firdous.galleryimageupload.delegates.UploadSuccessListener
import com.firdous.galleryimageupload.model.UploadModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask

class UploadImageViewModel (private val databaseReference: DatabaseReference, private val storageReference: StorageReference): ViewModel() {


    var errorMessage : MutableLiveData<String> = MutableLiveData()
    var progressUpdate : MutableLiveData<Double> = MutableLiveData()
    private var uploadSuccessListener : UploadSuccessListener? = null
    /**
     * upload image to Firebase storage and save url on realtime db
     */
    fun uploadImage(imageUrl : String?, extension : String?,uploadSuccessListener: UploadSuccessListener?){

        this.uploadSuccessListener = uploadSuccessListener
        val uriPath = Uri.parse("file://$imageUrl")
        val fileName = storageReference.child("${System.currentTimeMillis()} . $extension")
        fileName.putFile(uriPath).
            addOnSuccessListener {updateDatabase(fileName)}
            .addOnFailureListener{
                errorMessage.value = it.message
            }
            .addOnProgressListener {setUploadProgress(it)}
    }

    fun setUploadProgress(it: UploadTask.TaskSnapshot) {
       progressUpdate.value = (100.0 * it.bytesTransferred/it.totalByteCount)

    }

    private fun updateDatabase(snapshot: StorageReference){
        var uploadModel: UploadModel?
        snapshot.downloadUrl.addOnSuccessListener {
            uploadModel = UploadModel("file${System.currentTimeMillis()}",it.toString())
            val uploadId = databaseReference.push().key
            uploadId?.let { id ->
                databaseReference.child(id).setValue(uploadModel)
                    .addOnSuccessListener {
                        uploadSuccessListener?.onUploadSuccess()
                    }
                    .addOnFailureListener {ex->
                        errorMessage.value = ex.message
                    }
            }
        }

    }
}