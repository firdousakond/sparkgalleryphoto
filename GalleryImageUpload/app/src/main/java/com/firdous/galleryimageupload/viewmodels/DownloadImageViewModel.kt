package com.firdous.galleryimageupload.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.firdous.galleryimageupload.model.UploadModel
import com.google.firebase.database.*
import java.util.ArrayList

class DownloadImageViewModel(private val databaseReference: DatabaseReference) : ViewModel(){


    var uploadModelList : MutableLiveData<List<UploadModel>> = MutableLiveData()
    var errorMessage : MutableLiveData<String> = MutableLiveData()

    fun fetchImages(){

         databaseReference.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                errorMessage.value = error.message
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val listData : ArrayList<UploadModel> = ArrayList()
                for (data in snapshot.children){
                    val uploadModel = data.getValue(UploadModel::class.java)
                    uploadModel?.let { listData.add(it) }
                }
                uploadModelList.value = listData
            }
        })
    }
}