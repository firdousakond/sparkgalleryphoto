package com.firdous.galleryimageupload.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.view.KeyEvent
import android.webkit.MimeTypeMap

import com.firdous.galleryimageupload.R
import com.firdous.galleryimageupload.permission.AppPermission
import com.firdous.galleryimageupload.utils.AbstractActivity
import com.firdous.galleryimageupload.utils.AppConstant
import com.firdous.galleryimageupload.utils.CommonUtils
import com.firdous.galleryimageupload.utils.Logger
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView

import java.io.File
import java.util.ArrayList

import com.firdous.galleryimageupload.permission.PermissionUtil.*
import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE

class ImagePickerActivity : AbstractActivity() {
    private val tempPermissionList = ArrayList<String>()
    private val listRational = ArrayList<String>()
    private var cameraImageUri: Uri? = null
    private var extension: String? = ""
    private var requestedPermission: Int = 0

    companion object {

        private const val REQUEST_CAMERA_PIC = 100
        private const val REQUEST_GALLERY_PIC = 101
        private const val PERMISSION_REQUESTED_FOR_CAMERA = 1
        private const val PERMISSION_REQUESTED_FOR_GALLERY = 2
    }

    private val cameraImageName: String
        get() = System.currentTimeMillis().toString() + ".jpeg"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        showImagePickerDialog(
            REQUEST_CAMERA_PIC,
            REQUEST_GALLERY_PIC
        )

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Logger.debug("Config Change")
    }

    private fun checkForPermissionAndUseCamera(cameraActivityRequestCode: Int) {
        requestedPermission = PERMISSION_REQUESTED_FOR_CAMERA

        val appPermission = AppPermission.getInstance()
        val permission = intArrayOf(PERMISSION_REQUEST_CAMERA, PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE)

        if (!appPermission.requestPermissionList(this, permission, MULTIPLE_PERMISSION_REQUEST)) {
            takePictureFromCamera(cameraActivityRequestCode)
        }
    }

    private fun handlePermissionResult() {
        if (tempPermissionList.isNotEmpty()) {
            showPermissionAlert(true, false)
            checkPermissionAndShowPopup(tempPermissionList)
        } else if (listRational.isNotEmpty()) {
            showAlertToGoSetting(true, false)
            checkPermissionAndShowPopup(listRational)
        } else {
            if (requestedPermission == PERMISSION_REQUESTED_FOR_CAMERA) {
                takePictureFromCamera(REQUEST_CAMERA_PIC)
            } else {
                pickImageFromGallery(REQUEST_GALLERY_PIC)
            }
        }
    }

    private fun checkPermissionForGalleryAndProceed(galleryActivityRequestCode: Int) {
        requestedPermission = PERMISSION_REQUESTED_FOR_GALLERY
        val permissionRequestStorage = 109

        val appPermission = AppPermission.getInstance()
        val permission = intArrayOf(permissionRequestStorage)

        if (!appPermission.requestPermissionList(this, permission, MULTIPLE_PERMISSION_REQUEST)) {
            pickImageFromGallery(galleryActivityRequestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == MULTIPLE_PERMISSION_REQUEST) {
            // If request is cancelled, the result arrays are empty.

            if (grantResults.size > 0) {

                checkPermissionResult(grantResults, permissions)

                handlePermissionResult()
                // permission was granted, yay! Do the
                // contacts-related task you need to do.

            } else {
                finish()
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }

        }

    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun checkPermissionResult(results: IntArray, permissionsResult: Array<String>) {
        for (i in results.indices) {

            if (Build.VERSION.SDK_INT >= 23) {
                val showRationale = shouldShowRequestPermissionRationale(permissionsResult[i])

                if (results[i] != PackageManager.PERMISSION_GRANTED) {

                    if (!showRationale) {
                        listRational.add(permissionsResult[i])
                    } else {
                        tempPermissionList.add(permissionsResult[i])
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {
                CROP_IMAGE_ACTIVITY_REQUEST_CODE -> cropImage(data)

                REQUEST_CAMERA_PIC -> requestForCameraImage()

                REQUEST_GALLERY_PIC -> requestForGalleryImage(data!!)

                else -> {
                    Logger.debug("Not Matched")
                }
            }

        } else {
            finish()
        }
    }

    private fun cropImage(data: Intent?) {
        val result = CropImage.getActivityResult(data)
        var imageUri: Uri? = result.uri
        if (imageUri == null) {
            imageUri = cameraImageUri
        }
        sendResultData(imageUri!!.path, extension)
    }

    private fun requestForCameraImage() {
        if (cameraImageUri != null && !TextUtils.isEmpty(cameraImageUri!!.path)) {
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(File(cameraImageUri!!.path)).toString())
            sendToCropper(cameraImageUri, System.currentTimeMillis().toString() + "")
        }
    }

    private fun requestForGalleryImage(data: Intent) {
        val galleryUri = data.data
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(
            galleryUri!!,
            filePathColumn, null, null, null
        )
        cursor?.moveToFirst()
        cursor!!.close()

        val mime = MimeTypeMap.getSingleton()

        extension = mime.getExtensionFromMimeType(contentResolver.getType(galleryUri))
        sendToCropper(galleryUri, System.currentTimeMillis().toString() + "")
    }


    private fun sendResultData(imagePath: String?, extension: String?) {
        val resultIntent = Intent()
        resultIntent.putExtra(AppConstant.IMAGE_FILE, imagePath)
        resultIntent.putExtra(AppConstant.FILE_EXTENSION, extension)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    private fun showImagePickerDialog(cameraRequestCode: Int, galleryRequestCode: Int) {

        val builder = AlertDialog.Builder(this)

        val items: Array<String> = arrayOf(this.getString(R.string.pick_from_camera), this.getString(R.string.pick_from_gallery))

        builder.setTitle(this.getString(R.string.image_upload_title))

        builder.setItems(items) { _, which ->

            when (which) {
                0 -> {

                    Logger.debug("Camera is clicked")
                    checkForPermissionAndUseCamera(cameraRequestCode)
                }

                1 -> {

                    Logger.debug("Gallery is clicked")
                    checkPermissionForGalleryAndProceed(galleryRequestCode)
                }

                else -> {
                    Logger.debug("Not Matched")
                }
            }

        }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setOnKeyListener(KeyResponseListener())

        dialog.setOnCancelListener(CancelDialogListener())

        dialog.show()
    }

    private fun pickImageFromGallery(requestCode: Int) {

        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(i, requestCode)

    }

    private fun takePictureFromCamera(requestCode: Int) {
        val cameraIntent = Intent()

        cameraIntent.action = MediaStore.ACTION_IMAGE_CAPTURE

        val imageFileName = cameraImageName

        cameraImageUri = CommonUtils.getInstance().getTempUri(imageFileName, this)

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri)

        startActivityForResult(cameraIntent, requestCode)
    }

    private fun sendToCropper(uri: Uri?, imageName: String) {

        if (uri != null) {

            val intent = CropImage.activity(uri)
                .setAspectRatio(1, 1).setFixAspectRatio(true)
                .setGuidelines(CropImageView.Guidelines.OFF).setActivityTitle(getString(R.string.cropper))
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setOutputCompressQuality(50)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setOutputUri(CommonUtils.getInstance().getTempUri(imageName, this))
                .getIntent(this)
            startActivityForResult(intent, CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return false
    }

    private inner class CancelDialogListener : DialogInterface.OnCancelListener {
        override fun onCancel(dialog: DialogInterface) {
            finish()
        }
    }

    private inner class KeyResponseListener : DialogInterface.OnKeyListener {
        override fun onKey(dialog: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
            return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
                finish()
                true
            } else {
                false
            }
        }
    }

}
