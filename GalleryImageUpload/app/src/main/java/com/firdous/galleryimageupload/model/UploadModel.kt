package com.firdous.galleryimageupload.model

import com.google.firebase.database.PropertyName

data class UploadModel (
    @set:PropertyName("imageName")
    @get:PropertyName("imageName")
    var imageName: String = "",
    @set:PropertyName("imageUrl")
    @get:PropertyName("imageUrl")
    var imageUrl: String = ""
    )