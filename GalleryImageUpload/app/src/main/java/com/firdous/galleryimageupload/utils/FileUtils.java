package com.firdous.galleryimageupload.utils;

import java.io.File;

public class FileUtils {

    private FileUtils(){
        Logger.debug("Restrict Object creation");
    }
    public static File getFile(String path) {
        return new File(path);
    }

    public static File getFile(File dir, String path) {
        return new File(dir, path);
    }
}
