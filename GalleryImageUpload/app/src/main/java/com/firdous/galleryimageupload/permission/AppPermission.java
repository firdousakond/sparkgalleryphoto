package com.firdous.galleryimageupload.permission;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.annotation.IntDef;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import com.firdous.galleryimageupload.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

import static androidx.core.app.ActivityCompat.requestPermissions;
import static com.firdous.galleryimageupload.permission.PermissionUtil.getPermissionName;

public class AppPermission implements AppPermissionsInterface {

    static final int GRANTED = 0;
    static final int DENIED = 1;
    static final int BLOCKED_OR_NEVER_ASKED = 2;
    private static AppPermission appPermissionObject = new AppPermission();
    private PermissionAlertDialog twoButtonDialog;

    private AppPermission() {

    }

    public static AppPermission getInstance() {
        if (appPermissionObject == null) {
            appPermissionObject = new AppPermission();
        }
        return appPermissionObject;
    }

    @PermissionStatus
    public static int getPermissionStatus(Activity activity, String androidPermissionName) {
        return PermissionChecker.checkCallingOrSelfPermission(activity, androidPermissionName);
    }

    @Override
    public boolean requestPermissionList(Activity activity, int[] permissionCode, int requestCode) {
        boolean permissionFlag = false;
        ArrayList<String> permissionArray = new ArrayList<>();


        for (int aPermissionCode : permissionCode) {
            String permissionName = getPermissionName(aPermissionCode);
            if (ContextCompat.checkSelfPermission(activity, permissionName) != PackageManager.PERMISSION_GRANTED) {
                permissionArray.add(permissionName);
                permissionFlag = true;
            }
        }
        if (!permissionArray.isEmpty()) {

            addToPermissionArray(activity, permissionArray, requestCode);
        }

        return permissionFlag;
    }

    private void addToPermissionArray(Activity activity, ArrayList<String> permissionArray, int requestCode) {
        String[] tempArray = new String[permissionArray.size()];
        permissionArray.toArray(tempArray);
        permissionArray.clear();
        requestPermissions(activity, tempArray,
                requestCode);
    }

    @Override
    public boolean requestPermissionList(Activity activity, String[] permissionCode, int requestCode) {
        boolean permissionFlag = false;
        ArrayList<String> permissionList = new ArrayList<>();
        for (String selectedPermissionName : permissionCode) {
            if (ContextCompat.checkSelfPermission(activity, selectedPermissionName) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(selectedPermissionName);
                permissionFlag = true;
            }
        }
        if (!permissionList.isEmpty()) {

            addToPermissionArray(activity, permissionList, requestCode);
        }

        return permissionFlag;
    }

    @Override
    public void showPermissionDialog(Activity activity, AlertDialogProperties alertDialogProperties, String[] permissionNames) {

        showDialog(activity, alertDialogProperties, permissionNames);
    }

    @Override
    public void closeDialog() {
        if (twoButtonDialog != null && twoButtonDialog.isShowing()) {
            twoButtonDialog.dismiss();
        }
    }

    private void showDialog(Activity activity, AlertDialogProperties alertDialogProperties, String[] permissionNames) {

        StringBuilder message = new StringBuilder();

        for (int i = 0; i < permissionNames.length; i++) {

            switch (permissionNames[i]) {
                case Manifest.permission.CAMERA:
                    message.append(activity.getResources().getString(R.string.camera_permission));
                    break;
                case Manifest.permission.READ_EXTERNAL_STORAGE:
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    message.append(activity.getResources().getString(R.string.storage_permission));
                    break;
                default:
                    break;

            }

            String bottomSpace = i < (permissionNames.length - 1) ? "<br/><br/>" : "";
            message.append(bottomSpace);
        }
        alertDialogProperties.setMessage(message.toString());

        if (activity == null || activity.isFinishing()) {
            return;
        }

        twoButtonDialog = new PermissionAlertDialog(activity, alertDialogProperties);
        twoButtonDialog.show();
        twoButtonDialog.setCancelable(alertDialogProperties.isCancelable());
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({GRANTED, DENIED, BLOCKED_OR_NEVER_ASKED})
    public @interface PermissionStatus {
    }
}
