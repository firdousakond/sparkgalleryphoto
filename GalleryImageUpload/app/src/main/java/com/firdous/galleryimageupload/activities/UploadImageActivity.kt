package com.firdous.galleryimageupload.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.firdous.galleryimageupload.R
import com.firdous.galleryimageupload.delegates.UploadSuccessListener
import com.firdous.galleryimageupload.utils.AppConstant
import com.firdous.galleryimageupload.utils.AppConstant.Companion.IMAGE_FILE
import com.firdous.galleryimageupload.utils.CommonUtils
import com.firdous.galleryimageupload.utils.FileUtils
import com.firdous.galleryimageupload.utils.ImageUtils
import com.firdous.galleryimageupload.viewmodels.UploadImageViewModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_image_list.*
import kotlinx.android.synthetic.main.activity_toolbar.*
import kotlinx.android.synthetic.main.activity_upload_image.*

class UploadImageActivity : AppCompatActivity() {

    private var imagePath: String? = null
    private var extension: String? = null

    private lateinit var viewModel: UploadImageViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_image)
        val databaseReference = FirebaseDatabase.getInstance().getReference(AppConstant.UPLOADS)
        val storageReference = FirebaseStorage.getInstance().getReference(AppConstant.UPLOADS)
        viewModel = ViewModelProviders.of(this, CustomViewModelFactory(databaseReference,storageReference)).get(UploadImageViewModel::class.java)

        initUI()
        setListener()
    }

    private fun initUI() {
        ibtn_back.visibility = View.VISIBLE
        txt_header.text = getString(R.string.upload_image)
    }

    private fun setListener() {
        ibtn_back.setOnClickListener { finish() }
        img_edit.setOnClickListener { addPhoto() }
        btn_upload_photo.setOnClickListener { uploadNewPhoto() }
    }

    private fun uploadNewPhoto() {
        viewModel.errorMessage.observe(this, Observer {
            if (it != null) showError(it)
        })

        if (imagePath != null && extension != null) {
            viewModel.uploadImage(imagePath, extension, UploadImageSuccessListener())
            observeProgress()
        } else {
            CommonUtils.displaySnackbar(this, getString(R.string.empty_image_error), AppConstant.MESSAGE_TYPE_ERROR)
        }
    }

    private fun observeProgress() {
        viewModel.progressUpdate.observe(this, Observer {
            val percentage = "${it.toInt()}%"

            if (!progressbar.isVisible) {
                progressbar.visibility = View.VISIBLE
            }
            progressbar.progress = it.toInt()
            txt_percentage.text = percentage

        })

    }

    private fun showError(errorMessage: String) {
        progress_circular.visibility = View.GONE
        CommonUtils.displaySnackbar(this, errorMessage, AppConstant.MESSAGE_TYPE_ERROR)
    }


    private fun addPhoto() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        startActivityForResult(intent, AppConstant.IMAGE_PICKER_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null && requestCode == AppConstant.IMAGE_PICKER_CODE) {
            imagePath = data.getStringExtra(IMAGE_FILE)
            extension = data.getStringExtra(AppConstant.FILE_EXTENSION)
            setImage()
        }
    }

    private fun setImage() {
        ImageUtils.getInstance().loadImageFromFile(this, img_profile, FileUtils.getFile(imagePath))
    }

    override fun onSaveInstanceState(bundle: Bundle?) {
        super.onSaveInstanceState(bundle)
        bundle?.putString(AppConstant.IMAGE_URL, imagePath)
        bundle?.putString(AppConstant.FILE_EXTENSION,extension)
    }

    override fun onRestoreInstanceState(bundle: Bundle?) {
        super.onRestoreInstanceState(bundle)
        if (bundle?.getString(AppConstant.IMAGE_URL) != null) {
            imagePath = bundle.getString(AppConstant.IMAGE_URL)
            extension = bundle.getString(AppConstant.FILE_EXTENSION)
            ImageUtils.getInstance().loadImageFromFile(this, img_profile, FileUtils.getFile(imagePath))
        }
    }


    inner class UploadImageSuccessListener : UploadSuccessListener {
        override fun onUploadSuccess() {
            CommonUtils.displaySnackbar(this@UploadImageActivity, getString(R.string.image_uploaded_success), AppConstant.MESSAGE_TYPE_SUCCESS)
            val handler = Handler()
            handler.postDelayed({
                progressbar.visibility = View.GONE
                txt_percentage.text = ""
                finish()
            }, 1000)
        }

    }
    inner class CustomViewModelFactory (private val databaseReference: DatabaseReference, private val storageReference: StorageReference): ViewModelProvider.Factory{

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return UploadImageViewModel(databaseReference,storageReference) as T
        }
    }
}