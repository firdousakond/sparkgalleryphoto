package com.firdous.galleryimageupload.permission;

import android.app.Activity;

interface AppPermissionsInterface {

    boolean requestPermissionList(Activity activity, int[] permissionCode, int requestCode);

    boolean requestPermissionList(Activity activity, String[] permissionCode, int requestCode);

    void showPermissionDialog(Activity activity, AlertDialogProperties alertDialogProperties, String[] permissionNames);

    void closeDialog();
}
