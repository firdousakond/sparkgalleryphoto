package com.firdous.galleryimageupload.delegates

interface UploadSuccessListener {
    fun onUploadSuccess()
}