package com.firdous.galleryimageupload.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.content.FileProvider;
import com.firdous.galleryimageupload.BuildConfig;
import com.firdous.galleryimageupload.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;

public class CommonUtils {

    private static CommonUtils instance = null;

    public static CommonUtils getInstance() {
        if (instance == null) {
            instance = new CommonUtils();
        }
        return instance;
    }

    public Uri getTempUri(String temp, Activity activity) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

            return Uri.fromFile(getTempFile(temp, activity));


        } else {

            return getFileUriForNougat(activity, getTempFile(temp, activity));

        }
    }

    private Uri getFileUriForNougat(Activity activity, File file) {
        return FileProvider.getUriForFile(activity,
                BuildConfig.APPLICATION_ID + ".provider",
                file);
    }
    private File getTempFile(String temp, Activity activity) {


        if (isSDCARDMounted()) {

            File f;

            if (activity.getExternalCacheDir() != null) {
                f = new File(activity.getExternalCacheDir(), temp);

            } else {
                f = new File(activity.getCacheDir(), temp);
            }
            try {
                boolean fileCreated = f.createNewFile();
                if (fileCreated) {
                    Logger.debug("file is created");
                } else {
                    Logger.debug("file is not created");
                }
            } catch (IOException e) {
                Logger.error(e.getMessage());
            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted() {

        String status = Environment.getExternalStorageState();

        return status.equals(Environment.MEDIA_MOUNTED);

    }
    public static void displaySnackbar(Activity activity, String message, int messageType) {

        if (activity != null) {

            View view = activity.getWindow().getDecorView().getRootView();

            if (view == null) {
                return;
            }
            try {
                view = ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
            } catch (Exception e) {
                Logger.debug(e.getMessage());
            }

            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);

            TextView tvSnackBar = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);

            tvSnackBar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            switch (messageType) {

                case AppConstant.MESSAGE_TYPE_ERROR:
                    tvSnackBar.setBackgroundColor(activity.getResources().getColor(R.color.red_color));
                    break;
                case AppConstant.MESSAGE_TYPE_SUCCESS:
                    tvSnackBar.setBackgroundColor(activity.getResources().getColor(R.color.green_color));
                    break;
                default:
                    tvSnackBar.setBackgroundColor(activity.getResources().getColor(R.color.grey_color));

            }

            snackbar.getView().setPadding(0, 0, 0, 0);

            snackbar.show();

        }
    }


    public static boolean isNetworkAvailable(Activity activity) {

        ConnectivityManager connectivity = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {

            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null) {

                for (NetworkInfo anInfo : info) {

                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {

                        return true;
                    }
                }
            }
        }

        return false;

    }

}

