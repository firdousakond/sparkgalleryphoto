package com.firdous.galleryimageupload.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firdous.galleryimageupload.R
import com.firdous.galleryimageupload.activities.FullImageActivity
import com.firdous.galleryimageupload.model.UploadModel
import com.firdous.galleryimageupload.utils.AppConstant
import com.firdous.galleryimageupload.utils.ImageUtils
import kotlinx.android.synthetic.main.item_images.view.*

class ImageAdapter (var context: Context) : RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    private var uploadModelList : List<UploadModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_images, parent, false)
        return ImageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if(uploadModelList.isNotEmpty()){
            uploadModelList.size
        } else
            0
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
      holder.setView(uploadModelList[position])
    }

    fun updateImageList(uploadModelList:List<UploadModel>){
        this.uploadModelList = uploadModelList
        notifyDataSetChanged()
    }

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { gotoFullScreenImage() }
        }
        fun setView(uploadModel: UploadModel) {
            ImageUtils.getInstance().loadImageFromUrl(context,itemView.img_photo,uploadModel.imageUrl)
        }

        private fun gotoFullScreenImage() {
            val intent = Intent(context,FullImageActivity::class.java)
            intent.putExtra(AppConstant.IMAGE_URL,uploadModelList[adapterPosition].imageUrl)
            context.startActivity(intent)
        }

    }

}