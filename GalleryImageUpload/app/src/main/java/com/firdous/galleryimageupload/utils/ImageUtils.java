package com.firdous.galleryimageupload.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.firdous.galleryimageupload.R;

import java.io.File;


public class ImageUtils implements RequestListener<Drawable> {
    private static ImageUtils imageUtils;
    private final String tag = ImageUtils.this.getClass().getCanonicalName();

    private ImageUtils() {
    }

    public static synchronized ImageUtils getInstance() {
        if (imageUtils == null) {
            imageUtils = new ImageUtils();
        }
        return imageUtils;
    }


    private static boolean isValidContextForGlide(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            return !activity.isDestroyed() && !activity.isFinishing();
        }
        return true;
    }

    public void loadImageFromFile(@NonNull Context context, @NonNull ImageView imageView, @NonNull File filePath) {
        if (isValidContextForGlide(context)) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transform(new CenterCrop(), new RoundedCorners(5));
            Glide.with(context)
                    .load(filePath).apply(requestOptions)
                    .into(imageView);
        }
    }


    /**
     * Load IMAGE from url.
     *
     * @param context   the context
     * @param imageView the IMAGE view
     * @param url       the url
     */
    public void loadImageFromUrl(@NonNull Context context, @NonNull ImageView imageView, String url) {
        if (isValidContextForGlide(context)) {

            Logger.debug("Url: " + url);
            GlideUrl glideUrl = new GlideUrl(url);
            Glide.with(context)
                    .load(glideUrl).apply(new RequestOptions().timeout(AppConstant.CONSTANT_GLIDE_TIMEOUT).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.img_profile_pic).placeholder(R.drawable.progress_animation)).
                    listener(this)
                    .into(imageView);
        }

    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
        Log.e(tag, "Error img_loading IMAGE", e);
        return false;
    }

    @Override
    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
        Log.e(tag, "onResourceReady");
        return false;
    }

}
